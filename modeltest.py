#!/usr/bin/python3
import sys
print(sys.version)
assert(sys.version_info[0] == 3)

import model
import movement
import pprint
import dbg


mvmt = movement.FingerSingleJointAccDecMovement(0, 1, 1)
mvm2f = movement.FingerSingleJointAccDecMovement_gen(0, 1.6, 1)
mvm2 = movement.AccelerationSuperpositionMovement([(0, mvm2f)], 0, 1)

#double mvmt

mvmd = lambda s,a1,a2,d: movement.AccelerationSuperpositionMovement(movement.MJADM(s,a1,a2,d), 0, d+0.1)

#complex one



def run(maxt,*movements):
    for n,m in enumerate(movements):
        print ('\n'*3, '#'*20, n, '#'*20)
        finger = model.MovableFinger(lengths=[0.02,0.015,0.01], sensor_positions=[(p,m) for p in range(3) for m in (0,0.5,1)], init_angles=[0,0,0],  deltaT=0.01)
        finger.set_movement(m)

        yield dbg.run_simulation(finger, maxt)


mvmts = {
    'old':mvmt,
    'new': mvm2,
    'dual': mvmd(0, 1, 1.6, 1),
    'complex(0.2, 0.6)': movement.mvmc(0.2, 0.6),
    'complex(0.7, 0.3)': movement.mvmc(0.7, 0.3),
}
import matplotlib.pyplot as plt

def pltx(pname, style):
    for ser, lname in zip(results, names):
        dbg.plotvecs(plt.plot, dbg.mkvecs(pname,ser), linestyle=style, label=''+lname)

if __name__ == '__main__':

    results = list(run(None,*mvmts.values()))
    names = list(mvmts.keys())

    pltx('angles','dotted')
    pltx('speeds','dotted')

    plt.grid()
    plt.legend()
    plt.show()
