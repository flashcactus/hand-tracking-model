import numpy as np
import numpy.linalg as npla
import pprint
import dbg
import model

def weighted_metric(pos_weight, speed_weight):
    tovec = lambda s: np.array(list(s['angles']*pos_weight)+list(s['speeds']*speed_weight))
    def _metric(state1, state2):
        return npla.norm(tovec(state1) - tovec(state2))
    return _metric

pos_metric = weighted_metric(1,0)
vel_metric = weighted_metric(0,1)
l2_metric = weighted_metric(1,1)

maxdev_stat = max
mean_stat = np.mean
std_dev_stat = lambda seq:np.mean([i**2 for i in seq])**0.5


def gen_errors(errorspec, num_errors):
    '''
the errors parameter contains generators of offsets/dispersions
#this replaces the generators with their return values

errorspec := {
    error_type : {
        sensor_type: errorgenerator(),
        ...
    },
    ...
}
    '''
    return {etype: {
                    sens: errgen(num_errors)
                            for sens, errgen in espec.items()}
                    for etype, espec in errorspec.items()}


def mkerrorfinger(finger_params, errorspec=None, fingerclass=model.MovableFinger):
    if errorspec is not None:
        errors = gen_errors(errorspec, len(finger_params['sensor_positions']))
        finger_params['errors'] = errors
    return fingerclass(**finger_params)

class TestBench:
    def __init__(self, sourceFinger=None, destFinger=None, printout=False):
        self.install_fingers(sourceFinger,destFinger)
        self.print = printout


    def install_fingers(self, sourceFinger, destFinger):
        self.sourceFinger = sourceFinger
        self.destFinger = destFinger

    class Snapshot:
        '''single state of tested system'''
        def __init__(self, t, s_st, readings, d_st):
            self.t = t
            self.src_st = s_st
            self.dst_st = d_st
            self.readings = readings

        def __str__(self):
            return "STATE (t:{}): \n--- src:{}\n--- dst:{}\n--- readings:{}".format(self.t, pprint.pformat(self.src_st), pprint.pformat(self.dst_st), pprint.pformat(self.readings))

        def __repr__(self):
            return self.__str__()


    def run_test(self, movement):
        self.sourceFinger.reset_defaults()
        self.sourceFinger.set_movement(movement)
        self.destFinger.reset_defaults()
        try:
            while True:
                time = self.sourceFinger.time
                src_state = self.sourceFinger.state()
                readings = self.sourceFinger.step()
                self.destFinger.step(time, readings)
                dest_state = self.destFinger.state()

                if self.print:
                    print('\n\n[STEP] ', time )
                    pprint.pprint(src_state)
                    pprint.pprint(readings)
                    pprint.pprint(dest_state)

                yield self.Snapshot(time, src_state, readings, dest_state)
        except StopIteration:
            if self.print:
                print('\n================ [END] ===================\n')



    def run_tests(self, movements, metric=l2_metric, stats=[max]):
        for movement in movements:
            timeline = list(self.run_test(movement))
            errors = [metric(s.src_st, s.dst_st) for s in timeline]
            statvalues = [stat(errors) for stat in stats]
            yield timeline,errors,statvalues
