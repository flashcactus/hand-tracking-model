#!/usr/bin/python3
import pprint
import model, movement, algo, testbench
import math
import copy
import ft
import pdb
import time


def finger_pair(src_class, dst_class, mvmt, args, kwargs):
    src_finger = src_class(*args, **kwargs)
    src_finger.set_movement(mvmt)
    dst_finger = dst_class(*args, **kwargs)
    return src_finger, dst_finger

def standard_finger(deltaT=0.01, initial_position = [0,0,0]):
    return {
            'lengths': [0.02,0.015,0.01],
            'sensor_positions': [(p,m) for p in range(3) for m in (0.5,)],
            'init_angles': initial_position,
            'deltaT': deltaT
            }


#statnames = ['worst', 'mean', 'std_dev']
#stats = [max, testbench.mean_stat, testbench.std_dev_stat]

class SimpleTests:
    def __init__(self, movements, printout=False, errors=None, stats={'RMS': testbench.std_dev_stat}, fingens=None, **kwargs):
        self.printout=printout
        self.times, self.mvmts = tuple(zip(*movements))
        self.finger_params = standard_finger()
        self.statnames, self.stats = map(list,zip(*stats.items()))
        self.fingens = fingens

        #the errors parameter contains generators of offsets/dispersions
        #this replaces the generators with their return values
        self.errors = {etype: {sens: errgen(len(self.finger_params['sensor_positions'])) for sens, errgen in espec.items()} for etype, espec in errors.items()} if errors else None
        self.finger_params['errors'] = self.errors
        self.src_finger = model.MovableFinger(**self.finger_params)
        #expand dispersions to vector


    def test4finger(self, dest_finger):
        src_finger = self.src_finger
        tb = testbench.TestBench(src_finger, dest_finger, printout=self.printout)
        reft = time.time()
        tresults = list(tb.run_tests(self.mvmts, testbench.pos_metric, self.stats))
        passed = time.time() - reft
        #pprint.pprint({t:r[1] for t,r in zip(times,tresults)})
        titles, table = ['time']+self.statnames, list(zip(self.times, map(lambda v:v[2], tresults)))
        print(self.mk_table(titles,table))
        print('t:', passed)
        return {'tbl_titles':titles,'table':table,'full_results':tresults}

    @staticmethod
    def mk_table(titles, table):
        def _mkt():
            yield '\t\t'.join(titles)
            vfmt = lambda seq: '\t'.join(('{:4.2e}'.format(v) for v in seq))
            for time, res in table:
                yield '{:6.3f}:\t{}'.format(time, vfmt(res))
        return ' ' + '\n '.join(_mkt())

    def SlowFinger(self):
        extended_params = dict(self.finger_params)
        extended_params['phalange_class']=algo.SimpleSlowPhalange
        return algo.SimpleIntegratingFinger(**extended_params)


    def FastFinger(self):
        extended_params = dict(self.finger_params)
        extended_params['phalange_class']=algo.SimpleFastPhalange
        return algo.SimpleIntegratingFinger(**extended_params)


    def run(self, fingens=None):
        fingens = fingens or self.fingens
        if fingens is None:
            fingers = {'slow':algo.mkSlowFinger(**self.finger_params), 'fast':algo.mkFastFinger(**self.finger_params)}
        else:
            fingers = {k:v(**self.finger_params) for k,v in fingens.items()}

        def runtests(fingers):
            for fingername,finger in fingers.items():
                print()
                print(fingername)
                yield fingername,self.test4finger(finger)

        testresults = {fn:tr for fn,tr in runtests(fingers)}

        prepend = lambda s, seq: [s+v for v in seq]

        addreduce = ft.partial(ft.reduce, lambda x,y:x+y)

        gettime = ft.first
        getvals = ft.second

        tblhead = addreduce([prepend(name+': ', res['tbl_titles'][1:]) for name,res in testresults.items()], ['time'])
        tbrows = zip(*[r['table'] for r in testresults.values()])
        table = [[gettime(row[0])] + addreduce(map(getvals, row)) for row in tbrows]

        return tblhead, table, testresults


scale_gyro = lambda degps: degps / 180 * math.pi #deg/s to rad/s
scale_acc = lambda mg: mg * 9.8e-3 #mg to m*s**-2
scalenoise_gyro = lambda rms_deg: (scale_gyro(rms_deg))**2
scalenoise_acc = lambda rms_mg: (scale_acc(rms_mg))**2

noisemaker = lambda disp: lambda leng: [disp]*leng

_gen_offsets = lambda leng, wid, rfunc: [[rfunc(l,w) for w in wid] for l in leng]
offset_maker = lambda rfunc: lambda leng: _gen_offsets(range(leng), list(range(3)), rfunc)

scalerand = lambda scale, rand: lambda l,w: rand(l,w)*scale
sinrand = lambda speed, offset: lambda l,w: math.sin(l*11*speed + w*13*speed + offset)

def plot(titles,table,backend='Qt5Agg',saveto=None, scaledata=1, name=''):
    import matplotlib.pyplot as plt
    plt.switch_backend(backend)
    timelines = list(zip(*table))

    tlines = {title:tline for title,tline in zip(titles, timelines)}

    plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Длительность движения, с')
    plt.ylabel('Ошибка оценки,°')
    plt.suptitle(name)

    for tt in titles[1:]:
        plt.plot(tlines['time'], [d*scaledata for d in tlines[tt]], label=tt, linestyle=('dotted' if 'madg' in tt else 'dashed' if 'swit' in tt else 'solid'))#, color=color)

    myplot = plt.legend()
    if saveto:
        plt.savefig(saveto)
    else:
        plt.show()

    return myplot



def gen_simple_movements(joint=0, from_angle=-1, to_angle=1, min_time=0.1, num_times=7, exp_base=2, deltaT=0.01, mov_func=movement.FingerSingleJointAccDecMovement, **kwa):
    times = [min_time*exp_base**i for i in range(num_times)]
    mvmts = [mov_func(joint, to_angle-from_angle, t) for t in times]
    return zip(times, mvmts)

def simple_f(joint=0, from_angle=-1, to_angle=1, deltaT=0.01, mov_func=movement.FingerSingleJointAccDecMovement):
 return lambda t: mov_func(joint, to_angle-from_angle, t)

complex_f = lambda delay: ft.partial(movement.mvmc, delay)

def gen_movements(pfun, min_time=None, num_times=None, exp_base=None, **kwa):
    params = [min_time*exp_base**i for i in range(num_times)]
    return [(p, pfun(p)) for p in params]


if __name__ == "__main__":

    class mv_args:
        default_args = {
            'min_time' : 0.25,
            'num_times' : 8,
            'exp_base' : 2,
        }
        large_args = {
            'min_time' : 0.1,
            'num_times' : 24,
            'exp_base' : 10**(1/8.1),
        }
        small_args = {
            'min_time' : 0.5,
            'num_times' : 3,
            'exp_base' : 5,
        }
        simplemv = simple_f(0, 0, 1)
        simplemvn = simple_f(0, 0, 1, mov_func=movement.SJADM)
        complexmv = complex_f(0.5)


    mvargs = mv_args.default_args
    mvtype = mv_args.complexmv

    madg_ns=[5e-3, 2e-3, 1e-3]
    madgfingens = {'madg static {}'.format(n): algo.mkmkMadgFingerS(n) for n in madg_ns}

    common_params = {
        'fingens':ft.dunion(madgfingens,{
            'switching .25':algo.mkmkThreshFinger(0.25),
            'slow':algo.mkSlowFinger,
            'fast':algo.mkFastFinger,
        }),
        'stats':{
            #'worst':max,
            'RMS': testbench.std_dev_stat}
    }

    msr = sinrand(1,1)
    gyro_rand = scalerand(scale_gyro(0.1), msr)
    acc_rand = scalerand(scale_acc(7), msr) #uncalibrated is 70 (~3 deg error)

    var_params = {
     #   'no errors': dict(),
        'Только шум':{
            'errors':{
                'noise':{
                    'gyro':noisemaker(scalenoise_gyro(0.1)),
                    'accel':noisemaker(scalenoise_acc(8))}}},
        'Только ошибка нуля': {
            'errors' :{
                'offset':{
                    'gyro':offset_maker(gyro_rand),
                    'accel':offset_maker(acc_rand)}}},
        'Шум и ошибка нуля': {
            'errors':{
                'noise':{
                    'gyro':noisemaker(scalenoise_gyro(0.1)),
                    'accel':noisemaker(scalenoise_acc(8))},
                'offset':{
                    'gyro':offset_maker(gyro_rand),
                    'accel':offset_maker(acc_rand)}}},
    }


    tables = {}

    mktests = lambda mvt, al, **kwa: SimpleTests(gen_movements(mvt, **al), **kwa)


    def runtest(varp, runargs=dict()):
        st = mktests(mvtype, mvargs, **common_params, **varp)
        return st.run(**runargs)

    for tname,vp in var_params.items():
        print("== {} ==".format(tname))
        titles, tables[tname] = runtest(vp)

    import pickle
    pickle.dump(tables, open('longtables.p', 'wb'))

    for k,v in tables.items():
        plot(titles, v, name=k, scaledata=180/math.pi)
