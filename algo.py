import model,dbg
import numpy as np
import numpy.linalg as npla
import math
import ft
import itertools

import pprint


class OutOfPlaneError(Exception):
    pass


class SimpleIntegratingFinger(model.Finger):

    def __init__(self, *a, phalange_class=None, **kw):
        if phalange_class is None:
            raise ValueError("phalange_class must be defined")
        self.phalanges=[]
        super().__init__(*a, **kw)
        sensors_by_phalange = [[sl for sp, sl in self.sensor_positions if sp == i] for i in range(3)]

        self.phalange_class = phalange_class

        self.phalanges = [
            phalange_class(l, ang, spd, self.g, ps, self.deltaT)
            for l, ang, spd, ps in
            zip(
                self.lengths,
                self.abs_angles,
                self.abs_speeds,
                sensors_by_phalange
            )
        ]


    def setup(self, *args):
        self._reset(*args)
        self._calc_vectors()
        for p, ang, vel in zip(self.phalanges, self.abs_angles, self.abs_speeds):
            p.place(self.time, ang, vel)


    def step(self, time, readings):
        last_time = self.time
        readings_by_phalange = [[r for r,(sp,sl) in zip(readings,self.sensor_positions) if sp == i] for i in range(3)]

        #estimate
        prev_ang = 0
        prev_vel = 0
        #TODO: WAAAAAAAT
        prev_jnt_acc = self.g
        for ph, rd in zip(self.phalanges, readings_by_phalange):
            ph.step(time, rd, prev_ang, prev_vel, prev_jnt_acc)
            #TODO: do the previous-joint magic

        self.abs_angles = [ph.global_angle for ph in self.phalanges]
        self.abs_speeds = [ph.global_speed for ph in self.phalanges]
        #wrap up
        offzip = lambda seq: zip(itertools.chain([0],seq), seq)
        abs2rel = lambda seq: np.array([next-prev for prev,next in offzip(seq)])
        self.angles = abs2rel(self.abs_angles)
        self.speeds = abs2rel(self.abs_speeds)


def SlowGetAbsAngle(local_acc, global_base_acc):
    '''calculates angle of phalange relative to base (in case of no significant rotation present)'''
    inpl_base_acc = global_base_acc[1:]
    inpl_base_dir = inpl_base_acc / npla.norm(inpl_base_acc)
    inpl_local_acc = local_acc[1:] #TODO: least sq
    inpl_local_dir = inpl_local_acc / npla.norm(inpl_local_acc)
    rot_sin = float(np.cross(inpl_local_dir, inpl_base_dir))
    rot_cos = np.dot(inpl_local_dir, inpl_base_dir)
    return np.arctan2(rot_sin, rot_cos)



class BaseIntegratingPhalange:

    def __init__(self, length, init_angle=0, init_speed=0, global_g=[0,0,10], sensor_positions = [], time=0, deltaT=0.005):
        '''same as in finger, except for sensor positions (now a list of floats)'''
        self.length = length
        self.global_g = global_g
        self.sensor_positions = sensor_positions
        self.init_args = (time, init_angle, init_speed)
        self.place(*self.init_args)

    '''
    def _mkglobals(self, prev_angle, prev_speed):
        self.global_angle = prev_angle + self.local_angle
        self.global_speed = prev_speed + self.local_speed
    '''
    def reset_pos(self):
        self.place(*self.init_args)

    def place(self, time=0, init_angle=0, init_speed=0):
        self.global_angle = init_angle
        self.global_speed = init_speed
        self.time = time

    def _mklocals(self):
        self.local_g = model.transform_vector_xrot(self.global_g, -self.global_angle)

    def step(self, time, readings, prev_angle, prev_angvel, joint_acc):
        raise NotImplementedError()

#acc, gyro, time, dt, pos -> newpos, newspeed



class SimpleSlowPhalange(BaseIntegratingPhalange):

    def step(self, time, readings, prev_angle, *args):
        #extract the accelerations
        local_accs = [r[0] for r in readings]
        dt = time - self.time

        #calculate the angle
        AvgAngle = np.mean([SlowGetAbsAngle(r, self.global_g) for r in local_accs])

        estAngle = fullcircle_correction(AvgAngle, self.global_angle)

        #now record our findings
        angle_delta = estAngle - self.global_angle
        if time - self.time != 0:
            self.global_speed = angle_delta / (time - self.time)
        self.global_angle = estAngle
        self.time = time


class SimpleFastPhalange(BaseIntegratingPhalange):
    '''eulerian integration'''

    def step(self, time, readings, prev_angle, prev_angvel, joint_acc):
        prevtime = self.time
        dt = time - prevtime
        local_accs = [r[0] for r in readings]
        ang_vels = [r[1][0] for r in readings]#take x only

        mean_ang_vel = np.mean(ang_vels)
        cur_speed = mean_ang_vel #* math.sign(self.global_speed) #if angular velocity is unsigned
        #use mean of current and last velocity
        self.global_angle += np.mean([self.global_speed, cur_speed]) * dt
        self.global_speed = cur_speed
        self.time = time

def fullcircle_correction(estimate, prev_estimate):
    if abs(prev_estimate - estimate) > 5:
        print('.',end='')
        newest = estimate + math.copysign(math.pi*2, prev_estimate - estimate)
#        print(prev_estimate, estimate, newest, prev_estimate-newest)
        return newest
    else:
        return estimate

def slowstep_flat(acc, gyro, dt, known_pos, ref_g):
    known_angle = known_pos[0]
    estangle = SlowGetAbsAngle(acc, ref_g)
    correst = fullcircle_correction(estangle, known_angle)
    angle_delta = correst - known_angle
    speed = angle_delta / dt if dt != 0 else known_pos[1]
    return correst, speed


def faststep_flat(acc, gyro, dt, known_pos, ref_g):
    prev_speed = known_pos[1]
    cur_speed = gyro[0]
    estangle = known_pos[0] + np.mean([prev_speed, cur_speed]) * dt
    return estangle, cur_speed



def mkfusionp(estfunctions, fusionfun):
    '''constructs a phalange given:
    estfunctions: f(acc, gyro, dt, known_pos, ref_g) -> new_pos
    fusionfun: f([acc, gyro, dt, known_pos, ref_g], *estimates) -> fused_estimate'''
    class _FusionPhalange(BaseIntegratingPhalange):
        def step(self, time, readings, prev_angle, prev_angvel, joint_acc):
            prevtime = self.time
            dt = time - prevtime
            local_accs = [r[0] for r in readings]
            local_acc = local_accs[0]
            ang_vels = [r[1] for r in readings]
            ang_vel = ang_vels[0]

            mean_ang_vel = np.mean(ang_vels, 0)

            arglist = [
                local_acc,
                ang_vel,
                dt,
                (self.global_angle, self.global_speed),
                self.global_g
            ]

            estimates = [f(*arglist) for f in estfunctions]
            final_estimate = fusionfun(arglist, *estimates)

            self.time = time
            self.global_angle, self.global_speed = final_estimate

    return _FusionPhalange

def DecisionMetric(angacc, angvels, estimated_pos):
#    import pdb; pdb.set_trace()
    est_parasitic_acc = model.rod_end_acc(
        np.array([0]*3),
        npla.norm([angvels[0],angvels[1]]),
        angacc,
        np.array([0,1,0])) #length
    return npla.norm(est_parasitic_acc)

def mkfusionf_weighted(weight_fun):
    def ffun(arglist, slow_est, fast_est):
        acc, gyro, dt, prev_est, global_g = arglist
        angacc = (gyro[0] - prev_est[1]) / dt if dt != 0 else 0
        metric = DecisionMetric(angacc, gyro, prev_est)
        slow_weight, fast_weight = weight_fun(metric)
        return np.average([slow_est, fast_est], 0, [slow_weight, fast_weight])
    return ffun

def mkfusionf_true_switching(gyro_error_rate, accscalemul = 1/9.8):
    total_error_est = 0
    def swfusionfun(arglist, slow_est, fast_est):
        acc, gyro, dt, prev_est, global_g = arglist

        nonlocal total_error_est
        angacc = (gyro[0] - prev_est[1])/dt if dt !=0 else float('inf')
        slow_error=DecisionMetric(angacc, gyro, prev_est) * accscalemul
        int_error=total_error_est + gyro_error_rate*dt
        if slow_error < int_error:
            total_error_est = slow_error
            return slow_est
        else:
            total_error_est = int_error
            return fast_est
    return swfusionfun



def mkfusionf_madg(weight_fun):
    def ffun(arglist, slow_est, fast_est):
        acc, gyro, dt, prev_est, global_g = arglist
        angacc = (gyro[0] - prev_est[1]) / dt if dt != 0 else 0
        metric = DecisionMetric(angacc, gyro, prev_est)
        slow_weight, fast_weight = weight_fun(metric)
        slow_dir = np.sign(np.array(slow_est) - np.array(prev_est))
        return slow_dir * dt * slow_weight + fast_est
    return ffun


mkslowfastphalange = ft.partial(mkfusionp, (slowstep_flat, faststep_flat)) #just add a fusionfun

mkfusionf_switching = lambda thresh: mkfusionf_weighted(lambda m: (1,0) if m < thresh else (0,1))
mkfusionf_madglike_static = lambda gamma: mkfusionf_weighted(lambda m: (gamma,1))
mkfusionf_madg_static = lambda beta: mkfusionf_madg(lambda m: (beta,1))

mkTrueSwitchingPhalange = ft.comp(mkslowfastphalange, mkfusionf_true_switching)
mkSwitchingPhalange = ft.comp(mkslowfastphalange, mkfusionf_switching)
mkMadglPhalangeS = ft.comp(mkslowfastphalange, mkfusionf_madglike_static)
mkMadgPhalangeS = ft.comp(mkslowfastphalange, mkfusionf_madg_static)

#TODO: make this work
def mkfusionf_madglike_dynamic():
    def wfun(parasitic_acc):
        pass

    return mkfusionf_weighted(wfun)
#mkMadgPhalangeD = ft.comp(mkslowfastphalange, mkfusionf_madglike_dynamic)


SIFer = lambda phalange_class: ft.partial(SimpleIntegratingFinger, phalange_class=phalange_class)

mkSlowFinger = SIFer(SimpleSlowPhalange)
mkFastFinger = SIFer(SimpleFastPhalange)
mkmkThreshFinger = lambda thresh: SIFer(mkSwitchingPhalange(thresh))
mkmkMadgFingerS = lambda gamma: SIFer(mkMadgPhalangeS(gamma))
