import model
import numpy as np
import numpy.linalg
import numpy.random
import math
import itertools
import ft

'''new simple movement spec:
mvmtfactory(mvmt_params) -> movement
movement(time, dt, [fingerstate]) -> accelerations, delta_angles_2, delta_speeds
movement as described by fun always starts at t=0
delta_angles_2 is the second-order residual, i.e. anything after linearly integrating the current speeds'
'''

class BaseMovement:

    def __init__(self):
        pass

    def setup(self, movable):
        raise NotImplementedError()

    def step(self, movable, deltaT):
        raise NotImplementedError()

#old legacy class
class FingerSingleJointAccDecMovement(BaseMovement):
    def __init__(self, jointnum, target_angle, target_time):
        self.jnum = jointnum
        self.target_angle = target_angle
        self.target_time = target_time


    def setup(self, finger):
        #finger.calc_inertia()
        #moment = finger.jmoments[self.jnum]
        self.angacc = 4 * self.target_angle / self.target_time**2
        self.switchtime = self.target_time/2
        self.inittime = finger.time
        self.gettime = lambda : finger.time - self.inittime
        self.done = False

    def step(self, finger, deltaT):
        if self.done:
            raise StopIteration()

        def st(acc, dt):
            readings = finger.get_readings([acc * (i==self.jnum) for i in range(3)])
            finger.angles[self.jnum] += finger.speeds[self.jnum] * dt + acc * dt**2 / 2
            finger.speeds[self.jnum] += acc * dt
            return readings

        time = self.gettime()
        if time + deltaT <= self.switchtime:
            #print('accel')
            return st(self.angacc, deltaT)
        elif time >= self.switchtime and time+deltaT <= self.target_time:
            #print('decel')
            return st(-self.angacc, deltaT)
        else: #crossing a boundary
            if time < self.switchtime and time + deltaT > self.switchtime:
                #print('SWITCHING')
                readings = st(self.angacc, self.switchtime - time)
                st(-self.angacc, time+deltaT - self.switchtime)
                return readings
            elif time < self.target_time and time + deltaT > self.target_time:
                #print('finishing')
                return st(-self.angacc, self.target_time-time)
            elif time >= self.target_time and not self.done:
                #print('done')
                self.done = True
                return st(0, deltaT)
                #return finger.get_readings([0]*3)
            else:
                raise ValueError("wtf, invalid time: {}".format(time))

def zipsum(*seqs):
    return ft.mapl(sum, itertools.zip_longest(*filter(None, seqs)))

def superpose(*funs):
    return lambda t: np.array(ft.reduce(zipsum, ft.jmap(t, funs)))


def acc_simple_fun(acc, start, duration):
    end = start+duration
    x_t = lambda t: t**2 * acc / 2
    v_t = lambda t: t*acc
    final_v = v_t(duration)
    final_x = x_t(duration)
    def afn(time):
        if time < start:
            return 0, 0, 0
        else:
            t = time - start
            if t < duration:
                return [acc,
                        v_t(t),
                        x_t(t)]
            else:
                return [0,
                        final_v,
                        final_x + (t-duration) * final_v]
    return afn

def accdec_simple_fun(acc, start, duration):
    return superpose(acc_simple_fun(acc, start, duration/2), acc_simple_fun(-acc, start+duration/2, duration/2))

empty_step = lambda: [np.array([0]*3)]*3

def adf_crutch_diff(a,b):
    newv = a - b
    newv[0] = b[0] #b is the current one, lol
    return newv

def FingerSingleJointAccDecMovement_gen(jnum, target_angle, target_time):
    angacc = 4 * target_angle / target_time**2
    switchtime = target_time/2
    inittime = 0
    done = False
    accmask = np.array([0]*3)
    accmask[jnum] = 1

    adfun = accdec_simple_fun(angacc, 0, target_time)

    def _fsjadm_accfun(time, dt):
        dmovement = np.array(adf_crutch_diff(adfun(time+dt) , adfun(time)))
        dmovement.resize((3,1)) # I hate inplace operations
        return dmovement * accmask

    return _fsjadm_accfun

class AccelerationSuperpositionMovement(BaseMovement):
    def __init__(self, movement_specs, start_time, end_time=None):
        """movement_specs = [(starting_time, movement_spec_fun), ...]"""
        self.movement_specs = sorted(movement_specs, key=lambda v: v[0])
        self.end_time = end_time

    def setup(self, finger):
        self.finger = finger

    def step(self, finger, deltaT):
        step_vals = empty_step()
        si=0
        for stime, mfun in self.movement_specs:
            try:
                step_vals += mfun(finger.time - stime, deltaT)
            except StopIteration:
                si += 1
        #check if we're past the end
        if si == len(self.movement_specs) or self.end_time is not None and finger.time > self.end_time:
            raise StopIteration
        accs, dspeeds, dangs = step_vals
        readings = finger.get_readings(accs)
        finger.angles += dangs # + finger.speeds*deltaT
        finger.speeds += dspeeds
        return readings

def SJADM(j, ang, dur, start=0):
    return AccelerationSuperpositionMovement([(0, FingerSingleJointAccDecMovement_gen(j, ang, dur))], start, dur+0.1)

def MJADM(start, ang1, ang2, dur):
     '''descriptor for AD movement with both an MCP and an IP component'''
     return [
         (start, FingerSingleJointAccDecMovement_gen(0,ang1,dur)),
         (start, FingerSingleJointAccDecMovement_gen(1,ang2,dur)),
         (start, FingerSingleJointAccDecMovement_gen(2,ang2,dur)),]


def mvmc(delay,duration):
    '''complex movement, useful for algo benchmarking'''
    d = delay
    m1 = 1
    m2 = 2
    mul = duration/(m1+m2)
    m1s, m1d = d, m1*mul
    m2s, m2d = m1s+m1d, m2*mul
    end = m2s+m2d+0.1

    movs = [(m1s, FingerSingleJointAccDecMovement_gen(0, 0.5, m1d))] + MJADM(m2s, -2.1, -1.5, m2d)

    return AccelerationSuperpositionMovement(movs, 0, end)
