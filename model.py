#!/usr/bin/python3
import numpy as np
import numpy.linalg
import numpy.random
import math
import copy
import itertools
from itertools import accumulate as Iacc
import dbg
import random
import ft
import movement

def transform_vector_xrot(vec, angle):
    '''rotates a vector around local x axis'''
    trmat = np.matrix([[1,0,0],[0,math.cos(angle),math.sin(angle)],[0,-math.sin(angle),math.cos(angle)]])
    return np.array(vec*trmat)[0]


def rod_end_acc(prev_acc, angvel, angacc, vec):
    '''acceleration of the end of a rotating stick given acceleration of the other end and rotation parameters'''
    return prev_acc + np.cross(vec, angacc * np.array([1,0,0])) + angvel**2 * vec


class Finger:
    '''A kinematic model of a 3-DoF finger with inertial sensors attached.
    arguments:
        lengths: lengths of phalanges from proximal to distal (m)
        init_angles, init_speeds: angles in MCP, PIP, DIP  (rad, rad/s)
        g: total acceleration of base of finger (m/s^2)
        sensor_positions: list of (a,b) tuples where a is phalange number, b is offset as multiple of phalange length
        deltaT: simulation time step
    '''

    def __init__(self, lengths, init_angles=[0.0,0.0,0.0], init_speeds=[0.0,0,0], g=[0.0,0,10], sensor_positions = [], deltaT=0.005, errors = None):
        '''x=right, y=forward, z=up'''
        self.lengths = lengths
        self.sensor_positions = sensor_positions #(phalange number, position as fraction of length)
        self.sensor_phalanges = [p for p,r in sensor_positions] # sensor num -> sensor phalange
        self.g = g
        self.deltaT = deltaT

        self.init_defaults = init_angles, init_speeds, deltaT
        self.reset_defaults()
        self._calc_vectors()
        self.errors = errors

    def reset_defaults(self):
        self.setup(*self.init_defaults)


    def setup(self, angles, speeds, deltaT):
        self._reset(angles, speeds, deltaT)


    def _reset(self, angles, speeds, deltaT):
        self.angles = np.array(angles,np.float64)
        self.speeds = np.array(speeds,np.float64)
        self.deltaT = deltaT
        self.time = 0
        self._calc_vectors()


    def step(self, deltaT=None):
        self.time += deltaT or self.deltaT


    def _calc_vectors(self):
        abs_angles = []
        vecs = []
        cur_angl = 0
        cur_vec = numpy.array([0,0,0])
        for an, leng in zip(self.angles, self.lengths):
            cur_angl += an
            abs_angles.append(cur_angl)
            vecs.append(np.array([0,leng*math.cos(cur_angl), leng*math.sin(cur_angl)]))
        self.vecs = vecs
        self.abs_angles = np.array(abs_angles)
        self.abs_speeds = np.array(list(Iacc(self.speeds)))
        self.sensor_vectors = [vecs[vn] * vmul for vn, vmul in self.sensor_positions]


    def get_readings(self, angular_accelerations, errors=None):
        #print('axx', angular_accelerations)
        self._calc_vectors()
        base_acc = self.g

        #wrapper of rod_end_acc for accumulate
        na = lambda prev, aar: rod_end_acc(prev, *aar)

        abs_speeds = list(Iacc(self.speeds))
        abs_accs = list(Iacc(angular_accelerations))

        #accelerations at base joints of each phalange
        joint_accelerations = list(Iacc(
            itertools.chain([np.array(base_acc)], zip(abs_speeds, abs_accs, self.vecs)),
            na
            ))
        #print(joint_accelerations)

        sensor_accelerations = [rod_end_acc(joint_accelerations[sp], abs_speeds[sp], abs_accs[sp], sv) for sv, sp in zip(self.sensor_vectors, self.sensor_phalanges)]

        accel_readings = np.array([transform_vector_xrot(acc, -self.abs_angles[sp]) for acc,sp in zip(sensor_accelerations, self.sensor_phalanges)])

        accum_angv = list(Iacc(self.speeds))
        #print('AA', accum_angv)
        gyro_readings = np.array([np.array([accum_angv[sp],0,0]) for sp in self.sensor_phalanges])

        errors = errors or self.errors
        if errors:
            #add some white noise
            if "noise" in errors:
                n = errors['noise']

                #white gaussian uncorrelated noise
                make_wnoise = lambda disp: numpy.random.multivariate_normal([0]*len(disp), np.diag(disp), check_valid = "warn")
                def mknoise(name):
                    disps = n[name]
                    noise = np.array([make_wnoise([d]*3) for d in disps]) #each reading is a vec3, axes assumed to be identical
                    #print(noise)
                    return noise

                accel_readings += mknoise('accel')
                gyro_readings += mknoise('gyro')

            #add offset
            if "offset" in errors:
                o = errors['offset']
                accel_readings += o['accel']
                gyro_readings += o['gyro']

        #print('AR', accel_readings)
        #print('GR', gyro_readings)
        return list(zip(accel_readings, gyro_readings))


    def state(self):
        return copy.deepcopy({
                'angles':self.angles,
                'speeds':self.speeds,
                'vectors':self.vecs
        })



class SimpleWeightedFinger(Finger):

    def __init__(self, *args, **kwargs):
        self.linear_density = kwargs.get('linear_density', 0)
        super().__init__(*args, **kwargs)


    def calc_inertia(self):
        self._calc_vectors()
        masses = [l * self.linear_density for l in self.lengths]
        imoments_centered = [m*l**2/12 for m,l in zip(masses, self.lengths)]

        huygsteiner = lambda moment, mass, r: moment+mass*r**2
        j2pdist = lambda jnum, pnum: numpy.linalg.norm(sum(self.vecs[jnum:pnum]) + self.vecs[pnum]/2) #distance from joint to center of phalange
        relmoment = lambda jnum, pnum: huygsteiner(imoments_centered[pnum], masses[pnum], j2pdist(jnum, pnum)) #moment of a single phalange relative to joint
        jmoment = lambda jnum: sum((relative_phalange_moment(jnum, pnum) for pnum in range(jnum, 3))) #total moment of phalanges distal to joint
        self.masses = masses
        self.jmoments = [jmoment(i) for i in range(3)]


class MovableFinger(Finger):
    '''movement: an instance of BaseMovement'''

    def __init__(self, *args, **kwargs):
        self.movement = None
        super().__init__(*args, **kwargs)

    def set_movement(self, movement):
        self.movement = movement #kwargs['movement']
        self.movement.setup(self)

    #TODO: reset must reset movement as well

    def setup(self, angles, speeds, deltaT):
        self._reset(angles, speeds, deltaT)

    def step(self, deltaT=None):
        dt = deltaT or self.deltaT
        readings = self.movement.step(self, dt)
        self.time += dt
        return readings
