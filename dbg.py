import pprint
import functools
import itertools
import ft

def print_args(fun):
    @functools.wraps(fun)
    def _wrapper(*args, **kwargs):
        print('---------->', fun.__name__)
        pprint.pprint(args)
        pprint.pprint(kwargs)
        return fun(*args,**kwargs)
    return _wrapper


def print_result(fun):
    @functools.wraps(fun)
    def _wrapper(*args, **kwargs):
        result = fun(*args,**kwargs)
        print('--')
        pprint.pprint(result)
        print('<--------', fun.__name__, '\n')
        return result
    return _wrapper



def run_simulation(finger, max_time=None):
    while finger.time < max_time if max_time is not None else True:
        try:
            t = finger.time
            print('\n\n[STEP] ', t)
            print('state:')
            state = finger.state()
            pprint.pprint(state)

            print('readings:')
            readings = finger.step()
            pprint.pprint(readings)

            yield t,state,readings
        except StopIteration:
            break
    print('\n\n[FINAL] ', finger.time )
    pprint.pprint(finger.state())
    yield finger.time,finger.state(),None



def mkvecs(compname, series):
    '''takes time series from run_simulation and extracts one component'''
    repfun = lambda record: (record[0], record[1].get(compname))
    return map(repfun, series)

import matplotlib as mpl
import matplotlib.pyplot as plt

def plotvecs(plotfun, vecseq, vecname, compfmt='{}[{}]', compindexing=None, scaledata=None, **kwargs):
    '''vecseq := [(t, [val, val, ...]), ...]'''
    times,vecs = zip(*vecseq)
    components = zip(*vecs)
    if compindexing is None:
        compindexing = itertools.count()

    if scaledata is not None:
        scalefun = lambda x: x*scaledata
    else:
        scalefun = ft.identity

    for i,c in zip(compindexing,components):
        l = compfmt.format(vecname, i)
        plotfun(times, ft.mapl(scalefun,c), label=l, **kwargs)
